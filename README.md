# SpellCheckAsYouType (SCAYT) CKEditor 4 integration.

## Introduction
INTRODUCTION

CKEditor spell checker integration. SpellCheckAsYouType (SCAYT)
CKEditor addon. SCAYT is "installation-less", using the web 
services of WebSpellChecker.net.

## Installation

### Library
REQUIREMENTS

Note you need to use 
[libraries installer](https://github.com/balbuf/drupal-libraries-installer) 
extension.

Library (javascript) is now included in the module.

### Module
INSTALLATION

Run `composer require 'drupal/ckeditor_scayt:^1.0'` and enable module.

## Configuration
CONFIGURATION

* Head to `Configuration > Content authoring`, select text format with CKEditor.
* Add `scayt` button to CKEditor toolbar.

## Links

More info about [plugin](https://ckeditor.com/cke4/addon/scayt).
[SCAYT add on developer notes](
https://ckeditor.com/docs/ckeditor4/latest/guide/dev_howtos_scayt.html).

## CKEditor 5 support

Note, in CKEditor 5 spelling, grammar, and punctuation checking is a 
[premium (paid) feature](https://ckeditor.com/docs/ckeditor5/latest/features/spelling-and-grammar-checking.html).
